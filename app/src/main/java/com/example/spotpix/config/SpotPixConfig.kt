package com.example.spotpix.config

object SpotPixConfig {
    var backgroundShelfPix: String = ""
    var nameProject: String = ""
    var colorButtonProject: String = ""
    var cod_campanha_shelfpix: Int = 0
}